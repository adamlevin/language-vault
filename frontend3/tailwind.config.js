/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.vue'],
  darkMode: 'media',
  theme: {
    extend: {},
  },
  plugins: [],
}
