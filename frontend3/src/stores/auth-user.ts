import { defineStore } from "pinia";
import apiCall from "../utils/api";

const baseUrl = `${import.meta.env.VITE_API_URL}/users`;

interface State {
  token: string;
  status: string;
  hasLoadedOnce: boolean;
  email: string;
}

export const useAuthStore = defineStore("authUser", {
  state: (): State => ({
    token: localStorage.getItem("user-token") || "",
    status: "",
    hasLoadedOnce: false,
    email: "",
  }),
  getters: {
    getToken: (s) => s.token,
    isAuthenticated: (s) => !!s.token,
    authStatus: (s) => s.status,
  },
  actions: {
    async authRequest(user) {
      this.status = "loading";
      new Promise((resolve, reject) => {
        apiCall
          .post("/api/login/", user)
          .then((resp) => {
            localStorage.setItem("user-token", "success");
            localStorage.setItem("refresh-token", "success");
            this.authSuccess(resp);
            this.userRequest();
            resolve(resp);
          })
          .catch((err) => {
            this.authError(err);
            localStorage.removeItem("user-token");
            reject(err);
          });
      });
    },
    authLogout() {
      console.log("logout");
      this.token = "";
      new Promise((resolve, reject) => {
        localStorage.removeItem("user-token");
        resolve();
      });
    },
    authRefresh() {
      new Promise((resolve, reject) => {
        apiCall
          .post("/api/auth/refresh_token/", {
            refresh: localStorage.getItem("refresh-token"),
          })
          .then((resp: { data: any }) => {
            localStorage.setItem("user-token", resp.data.access);
            this.authSuccess(resp);
          });
      });
    },

    authSuccess(resp: { data: any }) {
      this.status = "success";
      console.log(resp.data);
      this.token = "success";
      this.hasLoadedOnce = true;
    },
    authError(resp: any) {
      console.log(resp);
      this.status = "error";
      this.hasLoadedOnce = true;
    },
    userRequest() {
      apiCall
        .get("/api/users/profile/")
        .then((resp: { data: any }) => {
          const profile = resp.data;
          this.email = profile.email;
        })
        .catch((resp: any) => {
          this.authError(resp);
          this.authLogout();
        });
    },
  },
});
