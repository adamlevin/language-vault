import axios from "axios";
import { createApp } from "vue";
import VueAxios from "vue-axios";
import App from "./App.vue";

// Vuetify
import "vuetify/styles";

import { loadFonts } from "./plugins/webfontloader";
import router from "./router";

import { createPinia } from "pinia";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import "./index.css";

const pinia = createPinia();

const vuetify = createVuetify({
  components,
  directives,
});

loadFonts();

createApp(App)
  .use(router)
  .use(vuetify)
  .use(pinia)
  .use(VueAxios, axios)
  .mount("#app");
