import axios from "axios";

import { useAuthStore } from "@/stores/auth-user";

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

// TODO: switch to const protocol = import.meta.env.MODE === "development" ? "http" : "https";
const protocol = "http";

const apiCall = axios.create({
  baseURL: `${protocol}://${import.meta.env.VITE_API_BASE_URL}`,
});

const handleRequest = (config) => {
  // Do something before each request is sent
  const c = config;

  // this cookie must be sent with each axios request
  // in order for POST / PUT /DELETE http methods to work
  const cookie = getCookie("csrftoken") || "";

  if (cookie) {
    c.headers["X-CSRFToken"] = cookie;
  }

  return c;
};

const handleRequestError = (error) => {
  Promise.reject(error);
};

apiCall.interceptors.request.use(handleRequest, handleRequestError);

function handleSuccess(response) {
  console.log("response", response);
  return { data: response.data };
}

function handleError(error) {
  const { logout } = useAuthStore();
  console.log("error", error.response.status);
  switch (error.response.status) {
    case 400:
      break;
    case 401:
    case 403:
      // Log out user
      console.log("unauthorized");
      logout();
      break;
    case 404:
      // Show 404 page
      break;
    case 500:
    case 503:
      break;
    case 504:
      break;
    default:
      // Unknown Error
      break;
  }
  return Promise.reject(error);
}
apiCall.interceptors.response.use(handleSuccess, handleError);

export const backgroundApiCall = axios.create({
  baseURL: `${protocol}://${import.meta.env.VITE_API_BASE_URL}`,
});

backgroundApiCall.interceptors.request.use(handleRequest, handleRequestError);
backgroundApiCall.interceptors.response.use(handleSuccess, handleRequestError);

export default apiCall;
