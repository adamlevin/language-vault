import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/text-list",
      name: "text list",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/TextListView.vue"),
    },
    {
      path: "/text/:textId",
      name: "text-detail",
      component: () => import("@/views/Text.vue"),
    },
    {
      path: "/add-text",
      name: "add-text",
      component: () => import("@/views/AddTextView.vue"),
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/LoginView.vue"),
    },
    {
      path: "/words/:wordId",
      name: "word-detail",
      component: () => import("@/components/WordDetail.vue"),
    },
  ],
});

export default router;
