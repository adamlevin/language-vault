#!/bin/bash

echo "running frontend/start_dev.sh"
# https://docs.npmjs.com/cli/cache
npm cache verify

# install project dependencies
npm install

mkdir -p node_modules/.cache && chmod -R 777 node_modules/.cache
mkdir -p node_modules/.vite && chmod -R 777 node_modules/.vite
mkdir -p node_modules/.vitest && chmod -R 777 node_modules/.vitest

# run the development server
npm run dev