from django.conf import settings
from django.db import models


# Create your models here.
class BaseModel(models.Model):
    """
    Base model to be used for tracking common information including:

        - Creation Date
        - Last Updated Date
        - Created by User
        - Updated by User

    https://docs.djangoproject.com/en/2.1/topics/db/models/#abstract-base-classes
    """

    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    modified_on = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        default=None,
        blank=True,
        editable=False,
        on_delete=models.SET_NULL,
        related_name='%(app_label)s_%(class)s_createdby',
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        return super(BaseModel, self).__init__(*args, **kwargs)

    class Meta:
        abstract = True
