import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class SymbolValidator(object):
    def __init__(self, special_characters=1):
        self.special_characters = special_characters

    def validate(self, password, user=None):
        if not re.findall(r'[()[\]{}|\\`~!@#$%^&*_\-+=;:\'",<>./?]', password):
            raise ValidationError(
                _(
                    "The password must contain at least 1 symbol: "
                    + "()[]{}|\\`~!@#$%^&*_-+=;:'\",<>./?"
                ),
                code='password_no_symbol',
            )

    def get_help_text(self):
        return _(
            f"Your password must contain at least {self.special_characters} special charater."  # noqa
        )


class UppercaseValidator(object):
    def __init__(self, uppercase_characters=1):
        self.uppercase_characters = uppercase_characters

    def validate(self, password, user=None):
        if not re.findall(r'[A-Z]', password):
            raise ValidationError(
                _(
                    "The password must contain at least 1 uppercase letter, A-Z."
                ),  # noqa
                code='password_no_upper',
            )

    def get_help_text(self):
        return _(
            f"Your password must contain at least {self.uppercase_characters} uppercase letter, A - Z."  # noqa
        )
