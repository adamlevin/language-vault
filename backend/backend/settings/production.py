import os

from .base import *  # noqa

# Site
EMAIL_USE_TLS = True

# AWS S3 Static Files

STATIC_URL = "/static/"
MEDIA_URL = "/media/"
FULL_DOMAIN_NAME = os.environ.get(
    "FULL_DOMAIN_NAME", "dev.languagesvault.com"
)  # noqa
AWS_S3_CUSTOM_DOMAIN = FULL_DOMAIN_NAME

STATIC_ROOT = f"//{FULL_DOMAIN_NAME}/{STATIC_URL}/"
MEDIA_ROOT = f"//{FULL_DOMAIN_NAME}/{MEDIA_URL}/"


# Logging

log_level = "INFO"

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {'console': {'class': 'logging.StreamHandler'}},
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),  # noqa
        },
        'portal': {
            'handlers': ['console'],
            'level': os.getenv('PORTAL_LOG_LEVEL', log_level),  # noqa
        },
    },
}

# Email

EMAIL_HOST_USER = os.environ.get('DJANGO_EMAIL_HOST_USER', None)  # noqa
EMAIL_HOST_PASSWORD = os.environ.get('DJANGO_EMAIL_HOST_PASSWORD', None)
