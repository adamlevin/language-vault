from collections import defaultdict
from functools import cache

import spacy
from pyrae import dle as spanish_dictionary
from rest_framework import generics, status, viewsets
from rest_framework.decorators import api_view
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response

from vocab.phrase.phrase_detecting import (
    convert_string_to_tuple,
    get_positions_for_phrase_in_text,
    get_words_from_starting_positions,
    ordered_words_from_phrase,
    select_or_create_phrase,
    select_phrase,
)

from .models import (
    Guess,
    Phrase,
    PhraseGuess,
    PhrasePart,
    SourceText,
    TokenWithContext,
    Word,
)
from .serializers import (
    SourceTextSerializer,
    TokenWithContextSerializer,
    WordSerializer,
    WordWithRelatedSerializer,
)


class WordList(generics.ListAPIView):
    queryset = Word.objects.all()
    serializer_class = WordSerializer

    def get_queryset(self):
        """
        This view should return a list of all the Words
        for the currently authenticated user.
        """
        user = self.request.user
        return Word.objects.filter(created_by=user)


class WordDetail(generics.RetrieveAPIView):
    serializer_class = WordSerializer

    def get_queryset(self):
        """
        This view should return a list of all the Words
        for the currently authenticated user.
        """
        user = self.request.user
        return Word.objects.filter(created_by=user)


class SourceTextViewSet(viewsets.ModelViewSet):
    paginator = LimitOffsetPagination()
    serializer_class = SourceTextSerializer

    def get_queryset(self):
        """
        This view should return a list of all the SourceText
        for the currently authenticated user.
        """
        user = self.request.user
        return SourceText.objects.filter(created_by=user)

    def post(self, request):
        serializer = SourceTextSerializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=request.user)
        return Response(
            {"id": serializer.data['id']}, status=status.HTTP_201_CREATED
        )


def get_phrases_in_sentence(word_ids, user):
    all_possible_phrases = Phrase.objects.filter(
        starting_word_id__in=word_ids, created_by=user
    )
    results = {}
    for phrase in all_possible_phrases:
        phrase_word_ids = ordered_words_from_phrase(phrase)
        all_positions = get_positions_for_phrase_in_text(
            tuple(phrase_word_ids), tuple(word_ids)
        )
        if not all_positions:
            # if it doesn't appear in the text, move on
            continue
        phrase_guesses = PhraseGuess.objects.filter(
            phrase=phrase, created_by=user
        )
        guesses = [p.text for p in phrase_guesses]

        results[phrase_word_ids] = guesses
    return results


def compare_sequences(iter1, iter2):
    # https://stackoverflow.com/questions/2345092/python-why-is-comparison-between-lists-and-tuples-not-supported
    iter1, iter2 = iter(iter1), iter(iter2)
    for i1 in iter1:
        try:
            i2 = next(iter2)
        except StopIteration:
            return False

        if i1 != i2:
            return False

    try:
        i2 = next(iter2)
    except StopIteration:
        return True

    return False


def get_sentence_with_groups(sentence, phrases):
    # sort a list of lists by length
    phrases = sorted(phrases, key=len, reverse=True)
    for phrase in phrases:
        for i in range(len(sentence)):
            print("compare", sentence[i:i + len(phrase)], phrase, sentence[i:i + len(phrase)] == phrase, type(sentence[i:i + len(phrase)]), type(phrase))
            if compare_sequences(sentence[i:i + len(phrase)], phrase):
                sentence[i:i + len(phrase)] = [tuple(phrase)]

    return sentence


def group_phrases_in_sentences(sentences, user):
    grouped_sentences = []
    all_phrases_with_guesses = []
    for sentence in sentences:
        phrases_with_guesses = get_phrases_in_sentence(sentence, user)
        new_sentence = get_sentence_with_groups(sentence, phrases_with_guesses.keys())
        grouped_sentences.append(new_sentence)
        all_phrases_with_guesses.append(phrases_with_guesses)
    return grouped_sentences, all_phrases_with_guesses


class SourceTextDetail(viewsets.ViewSet):
    def separate_sentences(self, data):
        sentences = []
        words = []
        all_words = {}
        for word in data:
            if (
                word['word']['text'] in ['.', '?', '!', '\n']
                or '\n' in word['word']['text']
            ):
                if words:
                    sentences.append(words)
                    words = []
            else:
                words.append(word["word"]["id"])
                all_words[word['word']['id']] = word
        return sentences, all_words

    def get(self, request, pk):
        tokens_in_text = TokenWithContext.objects.filter(
            source_text=pk,
            created_by=request.user,
        ).order_by('position_in_source_text')
        serializer = TokenWithContextSerializer(tokens_in_text, many=True)
        sentences, all_words = self.separate_sentences(serializer.data)
        sentences, phrases_with_guesses = group_phrases_in_sentences(sentences, request.user)
        phrases_with_guesses_str_keys = [{str(k): v for k, v in phrases_for_sentence.items()} for phrases_for_sentence in phrases_with_guesses]
        return Response({'sentences': sentences, 'words': all_words, 'phrases': phrases_with_guesses_str_keys})


@api_view(['GET'])
def word_in_context(request, pk):
    word = Word.objects.filter(created_by=request.user).get(pk=pk)
    tokens = TokenWithContext.objects.filter(
        word=word, created_by=request.user
    )
    serializer = TokenWithContextSerializer(tokens, many=True)
    return Response(serializer.data)


def create_words(text, user):
    # this is based on SourceText create words
    # this should probably be made more DRY
    nlp = spacy.load("es_core_news_sm")
    doc = nlp(text)
    words = []
    for position, token in enumerate(doc):
        word, _ = Word.objects.get_or_create(
            text=token.text.lower(),
            part_of_speech=token.pos_,
            morphology=token.morph,
            lemma=token.lemma_.lower(),
            created_by=user,
        )
        words.append(word)
    return words


@cache
def get_definition_for_word(word):
    return spanish_dictionary.search_by_word(word=word).to_dict()


@api_view(['GET'])
def word_definition(request, pk):
    word = Word.objects.filter(created_by=request.user).get(pk=pk)
    api_result = get_definition_for_word(word.text)
    result = []
    definitions = api_result.get('articles', [{'definitions': []}])[0].get(
        'definitions', []
    )
    for definition in definitions[:3]:
        examples = definition.get('examples')
        first_example = ''
        if examples:
            first_example = examples[0].get('text', '')

        definition_sentence = definition.get('sentence', {'text': ''})
        definition_text = definition_sentence.get('text', '')
        words = create_words(definition_text, request.user)
        serializer = WordWithRelatedSerializer(words, many=True)
        word_id_list = []
        all_words = {}
        for word in serializer.data:
            word_id_list.append(word["word"]["id"])
            all_words[word['word']['id']] = word
        guesses = Guesses.get_guesses_for_word_ids(
            [word.id for word in words], request.user
        )
        phrases = get_phrases_in_sentence(word_id_list, request.user)
        result.append(
            {
                'definition': definition_text,
                'sentence': word_id_list,
                'words': all_words,
                'phrases': phrases,
                'guesses': guesses,
                'example': first_example,
            }
        )

    return Response(result)


class Guesses(viewsets.ViewSet):
    @staticmethod
    def get_guesses_for_word_ids(words_in_text_id, user):
        # look up the guesses for each word
        guesses = Guess.objects.filter(
            word__in=words_in_text_id, created_by=user
        ).values_list('word_id', 'text')
        guesses_dict = defaultdict(list)
        for guess in guesses:
            guesses_dict[guess[0]].append(guess[1])
        return guesses_dict

    def get_guesses(self, source_text_pk, user):
        # figure out what words are in the text
        words_in_text_id = (
            TokenWithContext.objects.select_related('word')
            .filter(source_text=source_text_pk, created_by=user)
            .order_by('position_in_source_text')
            .values_list('word__id', flat=True)
        )
        guesses = self.get_guesses_for_word_ids(words_in_text_id, user)
        return guesses

    def post(self, request, pk):

        word = Word.objects.filter(created_by=request.user).get(pk=pk)
        print(pk, word)
        Guess.objects.filter(word=word, created_by=request.user).delete()
        form_data = request.data['guess']
        for guess in form_data:
            new_guess = Guess(
                word=word,
                text=guess,
                created_by=request.user,
            )
            new_guess.save()
        return Response()

    def get(self, request, source_text_pk):
        guesses = self.get_guesses(source_text_pk, request.user)
        return Response(guesses)


class Phrases(viewsets.ViewSet):

    def get_phrases(self, source_text_pk, user):
        '''
        For a given source_text, get all the phrases which have
        guesses for that source text

        Returns a dictionary which maps the positions in the source
        text to the guesses for that phrase
        '''

        word_ids_in_text = (
            TokenWithContext.objects.select_related('word')
            .filter(source_text=source_text_pk, created_by=user)
            .values_list('word__id', flat=True)
            .order_by('position_in_source_text')
        )

        # find every phrase that starts with any words that appear
        # in the text
        all_possible_phrases = Phrase.objects.filter(
            starting_word_id__in=word_ids_in_text, created_by=user
        )

        results = {}

        # for every possible phrase in the text, find all the positions
        # it appears in the text
        # if there are any positions, find all guesses for that phrase
        for phrase in all_possible_phrases:
            phrase_word_ids = ordered_words_from_phrase(phrase)
            all_positions = get_positions_for_phrase_in_text(
                phrase_word_ids, word_ids_in_text
            )
            if not all_positions:
                # if it doesn't appear in the text, move on
                continue
            phrase_guesses = PhraseGuess.objects.filter(
                phrase=phrase, created_by=user
            )
            guesses = [p.text for p in phrase_guesses]

            for positions in all_positions:
                results[str(positions)] = guesses

        return results

    def post(self, request, source_text_pk):
        form_data = request.data['phrases']
        # check keys added
        # check keys changed
        existing_phrases = self.get_phrases(source_text_pk, request.user)
        changes = {}
        form_data_set = set(form_data)
        existing_phrases_set = set(existing_phrases)
        added_phrases = form_data_set - existing_phrases_set
        in_both = existing_phrases_set & form_data_set
        for key in in_both:
            if set(form_data[key]) != set(existing_phrases[key]):
                print(
                    f"{existing_phrases[key]} changed to {form_data[key]} for {key}"  # noqa
                )
                changes[key] = form_data[key]
        for k, v in changes.items():
            phrase = select_phrase(
                tuple(
                    x.id
                    for x in get_words_from_starting_positions(
                        source_text_pk, convert_string_to_tuple(k)
                    )
                ),
                request.user,
            )
            PhraseGuess.objects.filter(
                phrase=phrase, created_by=request.user
            ).delete()
            if not form_data[k]:
                phrase.delete()

            for guess in form_data[k]:
                new_guess = PhraseGuess(
                    phrase=phrase,
                    text=guess,
                    created_by=request.user,
                )
                new_guess.save()
        for added_phrase in added_phrases:
            words = get_words_from_starting_positions(
                source_text_pk, added_phrase
            )
            phrase = Phrase(
                starting_word=words[0],
                created_by=request.user,
            )
            phrase.save()
            for i, word in enumerate(words):
                phrase_parts = PhrasePart(
                    phrase=phrase,
                    word=word,
                    order=i,
                    created_by=request.user,
                )
                phrase_parts.save()
            for guess in form_data[added_phrase]:
                new_guess = PhraseGuess(
                    phrase=phrase,
                    text=guess,
                    created_by=request.user,
                )
                new_guess.save()

        guesses = self.get_phrases(source_text_pk, request.user)

        return Response(guesses)

    def post_phrase_guess(self, request):
        word_ids = tuple(request.data['word_ids'])
        value = request.data['value']
        phrase = select_or_create_phrase(word_ids, request.user)
        PhraseGuess.objects.filter(
            phrase=phrase, created_by=request.user
        ).delete()

        for guess in value:
            new_guess = PhraseGuess(
                phrase=phrase,
                text=guess,
                created_by=request.user,
            )
            new_guess.save()

        return Response()

    def get(self, request, source_text_pk):
        return Response(self.get_phrases(source_text_pk, request.user))
