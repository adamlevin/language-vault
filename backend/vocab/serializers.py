from rest_framework import serializers

from . import models


class WordSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Word


class SourceTextSerializer(serializers.ModelSerializer):
    text_preview = serializers.SerializerMethodField(read_only=True)

    def get_text_preview(self, obj):
        if hasattr(obj, 'text'):
            return obj.text[:100]

    class Meta:
        fields = ('id', 'text', 'created_by', 'text_preview')
        read_only_fields = ('text_preview',)
        model = models.SourceText


class PhraseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Phrase


class WordWithRelatedSerializer(serializers.ModelSerializer):
    word = serializers.SerializerMethodField()
    related = serializers.SerializerMethodField()

    def get_word(self, obj):
        return WordSerializer(obj).data

    def get_related(self, obj):
        # get the lemma
        lemma = obj.lemma
        # find words with that lemma
        ids = (
            models.Word.objects.filter(lemma=lemma, created_by=obj.created_by)
            .exclude(id=obj.id)
            .values_list('id', flat=True)
        )
        guesses = models.Guess.objects.filter(word_id__in=ids)
        guesses_set = set(x.text for x in guesses)
        # list the guesses
        return list(guesses_set)

    class Meta:
        fields = (
            'word',
            'related',
        )
        model = models.Word


class TokenWithContextSerializer(serializers.ModelSerializer):
    word = WordSerializer()
    related = serializers.SerializerMethodField()

    def get_related(self, obj):
        # get the lemma
        lemma = obj.word.lemma
        # find words with that lemma
        ids = (
            models.Word.objects.filter(lemma=lemma, created_by=obj.created_by)
            .exclude(id=obj.word_id)
            .values_list('id', flat=True)
        )
        guesses = models.Guess.objects.filter(
            word_id__in=ids, created_by=obj.created_by
        )
        guesses_set = set(x.text for x in guesses)
        # list the guesses
        return list(guesses_set)

    class Meta:
        fields = (
            'word',
            'related',
            'position_in_source_text',
            'source_text_id',
        )
        model = models.TokenWithContext
