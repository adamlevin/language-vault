import re
from functools import cache

from vocab.models import Phrase, PhrasePart, TokenWithContext


def select_or_create_phrase(word_ids, user):
    phrase = select_phrase(word_ids, user)
    if not phrase:
        phrase = Phrase(
            starting_word_id=word_ids[0],
            created_by=user,
        )
        phrase.save()
        for i, word_id in enumerate(word_ids):
            phrase_parts = PhrasePart(
                phrase=phrase,
                word_id=word_id,
                order=i,
                created_by=user,
            )
            phrase_parts.save()
    return phrase


def select_phrase(words_ids, user):
    '''
    selects the phrase that has all and only the words listed
    '''
    # TODO: move to a model manager for Phrase
    phrases = Phrase.objects.filter(
        starting_word_id=words_ids[0], created_by=user
    )
    for phrase in phrases:
        if words_ids == ordered_words_from_phrase(phrase):
            return phrase
    return None


@cache
def get_positions_for_phrase_in_text(phrase, word_ids_in_text):
    first_word_positions = [
        i for i, x in enumerate(word_ids_in_text) if x == phrase[0]
    ]

    all_positions_in_text = []
    for first_word_position in first_word_positions:
        if phrase == tuple(
            word_ids_in_text[
                first_word_position : first_word_position + len(phrase)
            ]
        ):
            positions = tuple(
                range(
                    first_word_position,
                    first_word_position + len(phrase),
                )
            )
            all_positions_in_text.append(positions)
    return all_positions_in_text


def ordered_words_from_phrase(phrase):
    words = []
    for part in phrase.phrasepart_set.all():
        words.append(part.word.id)
    return tuple(words)


def convert_string_to_tuple(text):
    # converts a string like "(1, 2, 24)" to the tuple (1, 2, 24)"
    return tuple(re.findall(r"(\d+),?", text))


def get_words_from_starting_positions(source_text_pk, positions):
    if isinstance(positions, str):
        positions = convert_string_to_tuple(positions)
    tokens = (
        TokenWithContext.objects.select_related('word')
        .filter(
            source_text_id=source_text_pk,
            position_in_source_text__in=positions,
        )
        .order_by('position_in_source_text')
    )

    words = [token.word for token in tokens]

    return words
