from django.urls import path

from . import views

urlpatterns = [
    path('words/', views.WordList.as_view(), name='words'),
    path('words/<int:pk>/', views.WordDetail.as_view(), name='word'),
    path(
        'words/<int:pk>/guess',
        views.Guesses.as_view({"post": "post"}, name='guess-for-word'),
    ),
    path(
        'words/<int:pk>/in-context',
        views.word_in_context,
        name='use-in-context',
    ),
    path(
        'words/<int:pk>/definition', views.word_definition, name='definition'
    ),
    path(
        'source-text/',
        views.SourceTextViewSet.as_view({"get": "list", "post": "post"}),
        name='source-text-list',
    ),
    path(
        'source-text/<int:pk>/',
        views.SourceTextDetail.as_view({"get": "get"}),
        name='source-text',
    ),
    path(
        'guesses/source-text/<int:source_text_pk>/',
        views.Guesses.as_view({"get": "get"}),
        name="guesses-for-source-text",
    ),
    path(
        'phrases/guess/',
        views.Phrases.as_view({"post": "post_phrase_guess"}),
        name='phrase-guess',
    ),
    path(
        'phrases/source-text/<int:source_text_pk>/',
        views.Phrases.as_view({"get": "get", "post": "post"}),
        name='phrases-for-source-text',
    ),
]
