import re


def remove_extra_whitespace(text):

    clean_text = "\n".join([s.strip() for s in text.split("\n") if s])
    clean_text = re.sub(r'\n\s*\n', '\n\n', clean_text)
    return clean_text
