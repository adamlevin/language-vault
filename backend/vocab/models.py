import spacy
from core.models import BaseModel
from django.contrib.postgres.fields import ArrayField
from django.db import models

from vocab.cleaning.remove_tags import strip_html_tags
from vocab.cleaning.remove_whitespace import remove_extra_whitespace

# Load Spanish tokenizer, tagger, parser and NER
nlp = spacy.load("es_core_news_sm")


class SourceText(BaseModel):
    text = models.TextField()

    def create_words(self, text, created_by):
        doc = nlp(text)
        for position, token in enumerate(doc):
            word, _ = Word.objects.get_or_create(
                text=token.text.lower(),
                part_of_speech=token.pos_,
                morphology=token.morph,
                lemma=token.lemma_.lower(),
                created_by=created_by,
            )
            token_with_context = TokenWithContext(
                word=word,
                sentence=token.sent,
                ancestors=list(token.ancestors),
                children=list(token.children),
                subtree=list(token.subtree),
                position_in_source_text=position,
                source_text=self,
                created_by=created_by,
            )
            token_with_context.save()

    def save(self, *args, **kwargs):
        _state = self._state.adding
        self.text = strip_html_tags(self.text)
        self.text = remove_extra_whitespace(self.text)

        super().save(*args, **kwargs)
        if _state:

            self.create_words(self.text, self.created_by)


class Word(BaseModel):
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['text', 'part_of_speech', 'morphology', 'lemma', 'created_by'],
                name='unique_word',
            )
        ]
    text = models.TextField()
    part_of_speech = models.TextField()
    morphology = models.TextField()
    lemma = models.TextField(db_index=True)

    def __str__(self):
        return self.text


class Guess(BaseModel):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    confidence = models.FloatField(default=10.0)
    text = models.TextField()

    def __str__(self):
        return self.text


class TokenWithContext(BaseModel):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    sentence = models.TextField()
    ancestors = ArrayField(models.TextField())
    children = ArrayField(models.TextField())
    subtree = ArrayField(models.TextField())
    position_in_source_text = models.IntegerField()
    source_text = models.ForeignKey(SourceText, on_delete=models.CASCADE)

    def __str__(self):
        return self.word.text


class PhrasePart(BaseModel):
    order = models.IntegerField()
    word = models.ForeignKey(
        'Word', on_delete=models.CASCADE, related_name='phrase_word'
    )
    phrase = models.ForeignKey('Phrase', on_delete=models.CASCADE)

    class Meta:
        ordering = [
            'order',
        ]


class Phrase(BaseModel):
    starting_word = models.ForeignKey(
        Word, on_delete=models.CASCADE, related_name='phrase_starting_word'
    )
    words = models.ManyToManyField(Word, through=PhrasePart)

    def get_words(self):
        return self.words.order_by('phrase_word')


class PhraseGuess(BaseModel):
    phrase = models.ForeignKey(Phrase, on_delete=models.CASCADE)
    confidence = models.FloatField(default=10.0)
    text = models.TextField()

    def __str__(self):
        return self.text
