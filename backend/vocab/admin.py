from django.contrib import admin

from .models import (
    Guess,
    Phrase,
    PhraseGuess,
    PhrasePart,
    SourceText,
    TokenWithContext,
    Word,
)


class WordAdmin(admin.ModelAdmin):
    readonly_fields = (
        'created_on',
        'modified_on',
        'created_by',
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user

        obj.save()


admin.site.register(Word, WordAdmin)
admin.site.register(TokenWithContext)
admin.site.register(Guess)
admin.site.register(SourceText)

admin.site.register(Phrase)
admin.site.register(PhraseGuess)
admin.site.register(PhrasePart)
