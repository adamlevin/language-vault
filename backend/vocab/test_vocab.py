import pytest
from accounts.testing_utilities import login_with_user
from django.contrib.auth import get_user_model
from django.urls import reverse

from vocab.models import Guess, SourceText, TokenWithContext
from vocab.views import Guesses

User = get_user_model()


# add lyric

# post phrases: {0, 1, 2: ["it is never enough"]}

# get it back


@pytest.mark.django_db(transaction=False)
def test_save_a_phrase():
    client, user = login_with_user()

    form = {'phrases': {"(0, 1, 2)": ["it is never enough"]}}
    source_text = SourceText(
        text="Nunca es suficiente para mí", created_by=user
    )
    source_text.save()

    url = reverse(
        'phrases-for-source-text', kwargs={'source_text_pk': source_text.id}
    )
    response = client.post(url, form, format='json')
    print(response.data)
    # assert response.status_code == 201

    # see if the phrase/guess is retrievable

    response = client.get(url, form, format='json')
    print(response.data)
    assert response.data == form['phrases']


@pytest.mark.django_db(transaction=False)
def test_get_guesses_for_word_ids():
    user = User.objects.create_user(email='normal@user.com', password='foo')
    source_text = SourceText(
        text="Nunca es suficiente para mí", created_by=user
    )
    source_text.save()
    words_in_text_id = (
        TokenWithContext.objects.select_related('word')
        .filter(source_text=source_text)
        .order_by('position_in_source_text')
        .values_list('word__id', flat=True)
    )
    all_guesses = [[], [], ['enough'], ['for', 'to'], ['me']]
    for i, guesses in enumerate(all_guesses):
        for guess in guesses:
            Guess(
                word_id=words_in_text_id[i], text=guess, created_by=user
            ).save()
    result = Guesses.get_guesses_for_word_ids(words_in_text_id, user)
    result = [list(x) for x in result]
    assert result == all_guesses
