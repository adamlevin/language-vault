import json

from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth.hashers import check_password
from django.db.utils import IntegrityError
from django.http import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST
from rest_framework import permissions, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.tasks import send_password_recovery_email

from .serializers import UserSerializer

# from .tasks import send_password_recovery_email
from .utils.helpers import decode_reset_token, encoded_reset_token

User = get_user_model()

EXPIRED_PW_RESET_LINK_TEXT = (
    "The link has expired. " "Please request a new password reset link"
)


@require_POST
def logout_view(request):
    logout(request)
    return JsonResponse({"detail": "Logout Successful"})


@ensure_csrf_cookie
def login_set_cookie(request):
    """
    `login_view` requires that a csrf cookie be set.
    `getCsrfToken` in `auth.js` uses this cookie to
    make a request to `login_view`
    """
    return JsonResponse({"details": "CSRF cookie set"})


@require_POST
def login_view(request):
    """
    This function logs in the user and returns
    and HttpOnly cookie, the `sessionid` cookie
    """
    data = json.loads(request.body)
    email = data.get('email')
    password = data.get('password')
    if email is None or password is None:
        return JsonResponse(
            {"errors": {"__all__": "Please enter both username and password"}},
            status=400,
        )
    user = authenticate(email=email, password=password)
    if user is not None:
        login(request, user)
        return JsonResponse({"detail": "Success"})
    return JsonResponse({"detail": "Invalid credentials"}, status=400)


class Profile(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        serialized_user = UserSerializer(user)
        return Response(serialized_user.data)


class PasswordResetRequest(APIView):

    permission_classes = []

    def post(self, request):
        email = request.data.get('email')
        user = None
        try:
            user = User.objects.get(email=email)
        except Exception:
            pass
        if user:
            send_password_recovery_email.delay(
                user.id, encoded_reset_token(user.id)
            )
        return Response({"message": "Check your email."})


class PasswordResetAttempt(viewsets.ViewSet):
    permission_classes = []

    def check_token(self, request):
        token = request.data.get('token')
        payload = decode_reset_token(token)
        # if the token has expired, payload is `None`
        if not payload:
            return Response(
                {"success": False, "message": EXPIRED_PW_RESET_LINK_TEXT}
            )
        else:
            return Response({"success": True, "message": "Token is valid"})

    def post(self, request):
        token = request.data.get('token')
        password1 = request.data.get('password1')
        password2 = request.data.get('password2')
        if password1 != password2:
            return Response(
                {"success": False, "message": "Passwords don't match"}
            )

        payload = decode_reset_token(token)
        if not payload:
            return Response(
                {"success": False, "message": EXPIRED_PW_RESET_LINK_TEXT}
            )

        user_id = payload['user_id']
        user = User.objects.get(id=user_id)
        current_password = user.password

        if user:
            if check_password(password1, current_password):
                return Response(
                    {
                        "success": False,
                        "message": "Your new password "
                        "can't be your current password.",
                    }
                )
            user.set_password(password1)
            user.save()
            return Response({"success": True, "message": "Check your email."})

        return Response(
            {
                "success": True,
                "message": "There was an error processing your request.",
            }
        )


class RegisterUser(viewsets.ViewSet):
    permission_classes = []

    def post(self, request):
        email = request.data.get('email')
        password1 = request.data.get('password1')
        password2 = request.data.get('password2')
        if password1 != password2:
            return Response(
                {"success": False, "message": "Passwords don't match"}
            )

        try:
            User.objects.create_user(email=email, password=password1)
        except IntegrityError:
            return Response(
                {
                    "success": False,
                    "message": "This user already exists.",
                }
            )
        except Exception:
            return Response(
                {"success": False, "message": "An unknown error occurred."}
            )

        return Response(
            {
                "success": True,
                "message": "Account created.",
            }
        )
