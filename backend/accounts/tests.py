from backend.validators import SymbolValidator, UppercaseValidator
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.test import TestCase

User = get_user_model()


class UsersManagersTests(TestCase):
    def test_create_user(self):
        user = User.objects.create_user(
            email='normal@user.com', password='foo'
        )
        self.assertEqual(user.email, 'normal@user.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(user.username)
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email='')
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', password="foo")

    def test_create_superuser(self):
        User = get_user_model()
        admin_user = User.objects.create_superuser('super@user.com', 'foo')
        self.assertEqual(admin_user.email, 'super@user.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(admin_user.username)
        except AttributeError:
            pass
        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email='super@user.com', password='foo', is_superuser=False
            )


class PasswordValidatorTest(TestCase):
    def test_passord_validator(self):
        self.assertIsNone(UppercaseValidator().validate('Captializedpassord'))
        expected_error = (
            'The password must contain at least 1 uppercase letter, A-Z.'
        )
        with self.assertRaises(ValidationError) as cm:
            UppercaseValidator().validate('lowercasepassword')
        self.assertEqual(cm.exception.messages, [expected_error])

        self.assertIsNone(SymbolValidator().validate('Specialcharacters!'))
        expected_error = 'The password must contain at least 1 symbol: ()[]{}|\\`~!@#$%^&*_-+=;:\'",<>./?'  # noqa
        with self.assertRaises(ValidationError) as cm:
            SymbolValidator().validate('Nospecialchars')
        self.assertEqual(cm.exception.messages, [expected_error])
