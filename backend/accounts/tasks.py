import os

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage

User = get_user_model()


def send_password_recovery_email(self, user_id, token):
    user = User.objects.get(id=user_id)
    full_domain_name = os.environ.get("FULL_DOMAIN_NAME", "localhost")
    link = f"http://{full_domain_name}/password-reset-form/{token}/"
    email = EmailMessage(
        'Password Reset',
        f'Someone has requested a password change to your DrydenHQ account. '
        f'If this was you, click the following link '
        f'to reset your password. <br /><br />'
        f'<a href="{link}">{link}</a>',
        settings.FROM_EMAIL,
        [user.email],
    )
    email.content_subtype = "html"

    email.send()
