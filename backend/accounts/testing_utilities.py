"""
Utility functions for tests
"""


from django.contrib.auth import get_user_model
from rest_framework.test import APIClient

User = get_user_model()


def login_with_user():
    client = APIClient()
    email, password = 'admin@company.com', '5Mr6IUPOFjuL'
    user = User.objects.create_user(email=email, password=password)
    client.login(email=email, password=password)
    return client, user


def login():
    client, _ = login_with_user()
    return client
