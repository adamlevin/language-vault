# from django.contrib.auth.views import logout
from django.urls import path

from . import views

urlpatterns = [
    path('login-set-cookie/', views.login_set_cookie, name="login-set-cookie"),
    path('login/', views.login_view, name="login-view"),
    path('logout/', views.logout_view, name="logout-view"),
    path('users/profile/', views.Profile.as_view(), name='user-profile'),
    path(
        'accounts/password-reset-attempt/check-token/',
        views.PasswordResetAttempt.as_view({'post': 'check_token'}),
        name='reset-password-token',
    ),
    path(
        'accounts/password-reset-attempt/',
        views.PasswordResetAttempt.as_view({'post': 'post'}),
        name='reset-password-attempt',
    ),
    path(
        'accounts/reset-password/',
        views.PasswordResetRequest.as_view(),
        name='reset-password',
    ),
    path(
        'accounts/',
        views.RegisterUser.as_view({'post': 'post'}),
        name='register-user',
    ),
]
