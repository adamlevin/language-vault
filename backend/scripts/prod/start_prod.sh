#!/bin/bash

gunicorn -t 1000 -k gevent -w 4 -b 0.0.0.0:8000 backend.wsgi
