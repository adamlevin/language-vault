#!/usr/bin/env python3
import os

from aws_cdk import core as cdk

from awscdk.app_stack import ApplicationStack

environment_name = f"{os.environ.get('ENVIRONMENT', 'dev')}"

base_domain_name = os.environ.get("DOMAIN_NAME", "languagesvault.com")
base_app_name = os.environ.get("APP_NAME", "languagesvault-com")

full_app_name = f"{environment_name}-{base_app_name}"  # dev-mysite-com
full_domain_name = f"{environment_name}.{base_domain_name}"  # dev.mysite.com
aws_region = os.environ.get("AWS_DEFAULT_REGION", "us-east-1")


app = cdk.App()
ApplicationStack(
    app,
    f"{full_app_name}-stack",  # dev-mysite-com-stack
    environment_name=environment_name,
    base_domain_name=base_domain_name,
    full_domain_name=full_domain_name,
    base_app_name=base_app_name,
    full_app_name=full_app_name,
    env=cdk.Environment(region=aws_region),
)

app.synth()
