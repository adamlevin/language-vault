import json
import os

from aws_cdk import aws_cloudformation as cloudformation
from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_rds as rds
from aws_cdk import aws_secretsmanager as secrets
from aws_cdk import aws_ssm as ssm
from aws_cdk import core


class RdsStack(cloudformation.NestedStack):
    def __init__(
        self,
        scope: core.Construct,
        id: str,
        **kwargs,
    ) -> None:
        super().__init__(scope, id, **kwargs)

        self.db_security_group = ec2.CfnSecurityGroup(
            self,
            "DBSecurityGroup",
            group_description=f"{scope.full_app_name}-db-security-group",
            group_name=f"{scope.full_app_name}-db-security-group",
            vpc_id=scope.vpc.vpc_id,
            security_group_ingress=[
                ec2.CfnSecurityGroup.IngressProperty(
                    ip_protocol="tcp",
                    from_port=5432,
                    to_port=5432,
                    source_security_group_id=scope.vpc.vpc_default_security_group,
                )
            ],
        )

        self.db_subnet_group = rds.CfnDBSubnetGroup(
            self,
            "DBSubnetGroup",
            db_subnet_group_description=f"{scope.full_app_name} DB Subnet Group",
            db_subnet_group_name=f"{scope.full_app_name}-db-subnet-group",
            subnet_ids=[x.subnet_id for x in scope.vpc.isolated_subnets],
        )

        self.db_parameter_group = rds.CfnDBParameterGroup(
            self,
            "DBParameterGroup",
            description=f"{scope.full_app_name} DB Parameter Group",
            family="postgres10",
            parameters={
                "shared_preload_libraries": 'pg_stat_statements',
                "pg_stat_statements.max": '10000',
                "pg_stat_statements.track": 'all',
                "log_min_duration_statement": '1000',
                "log_duration": 'on',
                "random_page_cost": '1.1',
                "checkpoint_completion_target": '0.9',
                "min_wal_size": '80',
                "effective_io_concurrency": '200',
                "log_statement": 'all',
            },
        )

        self.rds_instance = rds.CfnDBInstance(
            self,
            "DBInstance",
            db_instance_class=os.environ.get(
                "RDS_INSTANCE_SIZE", "db.t2.micro"
            ),
            engine="postgres",
            engine_version="10.15",
            allocated_storage="100",
            auto_minor_version_upgrade=False,
            db_parameter_group_name=self.db_parameter_group.ref,
            db_snapshot_identifier=os.environ.get("DB_SNAPSHOT_ID", ""),
            enable_performance_insights=True,
            master_username="postgres",
            master_user_password=os.environ.get("POSTGRES_PASSWORD"),
            port="5432",
            publicly_accessible=True,
            storage_type="gp2",
            db_subnet_group_name=self.db_subnet_group.db_subnet_group_name,
            vpc_security_groups=[
                self.db_security_group.get_att("GroupId").to_string()
            ],
        )
