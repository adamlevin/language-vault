from aws_cdk import aws_cloudformation as cloudformation
from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_ecs as ecs
from aws_cdk import core


class EcsStack(cloudformation.NestedStack):
    def __init__(
        self,
        scope: core.Construct,
        id: str,
        **kwargs,
    ) -> None:
        super().__init__(scope, id, **kwargs)

        self.cluster = ecs.Cluster(
            self,
            "EcsCluster",
            vpc=scope.vpc,
            cluster_name=f"{scope.full_app_name}-cluster",
        )
