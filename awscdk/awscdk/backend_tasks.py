import os

from aws_cdk import aws_cloudformation as cloudformation
from aws_cdk import aws_ecs as ecs
from aws_cdk import aws_logs as logs
from aws_cdk import core

# These tasks are executed from manual GitLab CI jobs. The cluster is
# specified with:
# `aws ecs run-task --cluster ${ENVIRONMENT}-${APP_NAME}-cluster [...]`


class BackendTasksStack(cloudformation.NestedStack):
    def __init__(
        self,
        scope: core.Construct,
        id: str,
        **kwargs,
    ) -> None:
        super().__init__(
            scope,
            id,
            **kwargs,
        )

        # migrate
        self.migrate_task = ecs.FargateTaskDefinition(
            self, "MigrateTask", family=f"{scope.full_app_name}-migrate"
        )

        self.migrate_task.add_container(
            "MigrateCommand",
            image=scope.image,
            environment=scope.variables.regular_variables,
            command=["python3", "manage.py", "migrate", "--no-input"],
            logging=ecs.LogDrivers.aws_logs(
                stream_prefix="MigrateCommand",
                log_retention=logs.RetentionDays.ONE_DAY,
            ),
        )

        # collectstatic
        self.collectstatic_task = ecs.FargateTaskDefinition(
            self,
            "CollecstaticTask",
            family=f"{scope.full_app_name}-collectstatic",
        )

        scope.backend_assets_bucket.grant_read_write(
            self.collectstatic_task.task_role
        )

        self.collectstatic_task.add_container(
            "CollecstaticCommand",
            image=scope.image,
            environment=scope.variables.regular_variables,
            command=["python3", "manage.py", "collectstatic", "--no-input"],
            logging=ecs.LogDrivers.aws_logs(
                stream_prefix="CollectstaticCommand",
                log_retention=logs.RetentionDays.ONE_DAY,
            ),
        )

        # createsuperuser
        self.create_superuser_task = ecs.FargateTaskDefinition(
            self,
            "CreateSuperuserTask",
            family=f"{scope.full_app_name}-create-superuser",
        )

        self.create_superuser_task.add_container(
            "CreateSuperuserCommand",
            image=scope.image,
            environment=scope.variables.regular_variables,
            command=["python3", "manage.py", "create_default_user"],
            logging=ecs.LogDrivers.aws_logs(
                stream_prefix="CreateSuperuserCommand",
                log_retention=logs.RetentionDays.ONE_DAY,
            ),
        )
