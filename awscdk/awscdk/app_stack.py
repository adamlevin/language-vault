import os

from aws_cdk import aws_certificatemanager as acm
from aws_cdk import aws_ecs as ecs
from aws_cdk import aws_s3_deployment as s3_deployment
from aws_cdk import core as cdk

from alb import AlbStack
from backend import BackendServiceStack
from backend_assets import BackendAssetsStack
from backend_tasks import BackendTasksStack

# from bastion_host import BastionHost
from cloudfront import CloudFrontStack
from ecs import EcsStack
from env_vars import Variables
from hosted_zone import HostedZone
from rds import RdsStack
from static_site_bucket import StaticSiteStack
from vpc import VpcStack


class ApplicationStack(cdk.Stack):
    def __init__(
        self,
        scope: cdk.Construct,
        id: str,
        environment_name: str,
        base_domain_name: str,
        full_domain_name: str,
        base_app_name: str,
        full_app_name: str,
        **kwargs
    ) -> None:
        super().__init__(scope, id, **kwargs)

        self.environment_name = environment_name
        self.base_domain_name = base_domain_name
        self.full_domain_name = full_domain_name
        self.base_app_name = base_app_name
        self.full_app_name = full_app_name

        self.hosted_zone = HostedZone(self, "HostedZone").hosted_zone

        self.certificate = acm.Certificate.from_certificate_arn(
            self, "WildCardCert", os.environ.get("CERT_ARN")
        )

        self.vpc_stack = VpcStack(self, "VpcStack")
        self.vpc = self.vpc_stack.vpc

        self.alb_stack = AlbStack(self, "AlbStack")
        self.alb = self.alb_stack.alb

        self.https_listener = self.alb_stack.https_listener

        self.static_site_stack = StaticSiteStack(self, "StaticSiteStack")
        self.static_site_bucket = self.static_site_stack.static_site_bucket

        self.backend_assets = BackendAssetsStack(self, "BackendAssetsStack")
        self.backend_assets_bucket = self.backend_assets.assets_bucket

        self.cloudfront = CloudFrontStack(self, "CloudFrontStack")

        if os.path.isdir("./frontend/dist"):
            s3_deployment.BucketDeployment(
                self,
                "BucketDeployment",
                destination_bucket=self.static_site_bucket,
                sources=[s3_deployment.Source.asset("./frontend/dist")],
                distribution=self.cloudfront.distribution,
            )

        self.ecs = EcsStack(self, "EcsStack")
        self.cluster = self.ecs.cluster

        self.rds = RdsStack(self, "RdsStack")

        # image used for all django containers: gunicorn
        self.image = ecs.AssetImage(
            "./backend", file="scripts/prod/Dockerfile"
        )

        self.variables = Variables(self, "Variables")

        self.backend_service = BackendServiceStack(
            self, "BackendApiServiceStack"
        )

        self.backend_tasks = BackendTasksStack(self, "BackendTasksStack")
        # self.bastion_host = BastionHost(self, "BastionHost")
