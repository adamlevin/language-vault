import os

from aws_cdk import core


class Variables(core.Construct):
    def __init__(
        self,
        scope: core.Construct,
        id: str,
        **kwargs,
    ) -> None:
        super().__init__(
            scope,
            id,
            **kwargs,
        )

        self.regular_variables = {
            "DJANGO_SETTINGS_MODULE": "backend.settings.production",
            "DEBUG": "",
            "ENVIRONMENT": scope.environment_name,
            "FULL_DOMAIN_NAME": scope.full_domain_name,
            "FULL_APP_NAME": scope.full_app_name,
            "AWS_STORAGE_BUCKET_NAME": scope.backend_assets_bucket.bucket_name,
            "POSTGRES_SERVICE_HOST": scope.rds.rds_instance.get_att(
                "Endpoint.Address"
            ).to_string(),
            "POSTGRES_PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
            "DJANGO_EMAIL_HOST_USER": os.environ.get("DJANGO_EMAIL_HOST_USER"),
            "DJANGO_EMAIL_HOST_PASSWORD": os.environ.get(
                "DJANGO_EMAIL_HOST_PASSWORD"
            ),
            "SECRET_KEY": os.environ.get("SECRET_KEY"),
            "API_VERSION": os.environ.get(
                "CI_COMMIT_TAG", "API_VERSION_UNKNOWN"
            ),
        }
