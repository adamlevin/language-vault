#!/bin/bash

echo "running frontend/start_dev.sh"
# https://docs.npmjs.com/cli/cache
npm cache verify

# install project dependencies
npm install

# run the development server
npm run serve