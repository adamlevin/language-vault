import ElementUI from "element-ui";
import locale from "element-ui/lib/locale/lang/en";
import "element-ui/lib/theme-chalk/index.css";
import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import CenterContainer from "./components/lib/center-container";
import Loading from "./components/lib/loading";
// import "./registerServiceWorker";
import router from "./router";
import store from "./store";

Vue.use(ElementUI, { locale });
Vue.use(VueRouter);
Vue.config.productionTip = false;
Vue.component("loading", Loading);
Vue.component("center-container", CenterContainer);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
