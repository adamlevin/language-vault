/* eslint-disable */
import Vue from "vue";
import Router from "vue-router";
import Login from "../components/login";
import Words from "../components/Words";
import store from "../store";

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/");
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/login");
};

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      component: () => import("@/components/Home.vue")
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: "/signup",
      name: "SignUp",
      component: () => import("@/components/SignUp.vue"),
      beforeEnter: ifNotAuthenticated
    },

    {
      path: "/password-reset",
      name: "PasswordReset",
      component: () => import("@/components/login/PasswordReset.vue"),
      beforeEnter: ifNotAuthenticated
    },
    {
      path: "/password-reset-form/:token",
      name: "PasswordResetForm",
      component: () => import("@/components/login/PasswordResetForm.vue"),
      beforeEnter: ifNotAuthenticated
    },
    {
      path: "/logout",
      name: "Logout",
      component: () => import("@/components/Logout.vue"),
      beforeEnter: ifAuthenticated
    },
    {
      path: "/words",
      name: "Words",
      component: Words,
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: "/words/:wordId",
          name: "word-detail",
          component: () => import("@/components/WordDetail.vue"),
          beforeEnter: ifAuthenticated
        }
      ]
    },
    {
      path: "/text/",
      name: "text-list",
      component: () => import("@/components/text/TextList.vue"),
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: "/text/add/",
          name: "add-text",
          component: () => import("@/components/text/AddText.vue"),
          beforeEnter: ifAuthenticated
        },
        {
          path: "/text/:textId",
          name: "text-detail",
          component: () => import("@/components/text/Text.vue"),
          beforeEnter: ifAuthenticated
        }
      ]
    }
  ]
});
