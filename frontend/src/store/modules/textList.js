/* eslint-disable no-shadow, no-param-reassign  */
import apiCall from "../../utils/api";

const state = {
  textList: null,
  count: null,
  loading: true,
  paginationLimit: 5,
  currentPage: 1,
  reportId: null,
  selectedStatuses: []
};

const getters = {
  get: s => s.textList,
  getLoading: s => s.loading,
  getCount: s => s.count,
  getPaginationLimit: s => s.paginationLimit,
  getCurrentPage: s => s.currentPage,
  getSelectedStatuses: s => s.selectedStatuses,
  getQuery: s => ({
    limit: s.paginationLimit,
    offset: (s.currentPage - 1) * s.paginationLimit,
    text_id: s.textId,
    statuses: s.selectedStatuses.join(",")
  })
};

const actions = {
  fetchData: ({ commit, getters }) => {
    commit("setLoading", { loading: true });
    const form = getters.getQuery;
    apiCall.get(`/api/source-text/`, { params: form }).then(resp => {
      commit("fetchData", resp.data);
    });
  },
  setCurrentPage: ({ commit, dispatch }, payload) => {
    commit("setCurrentPage", payload.p);
    dispatch("fetchData", { id: payload.id });
  },
  setPaginationLimit: ({ commit, dispatch }, { value, id }) => {
    commit("setPaginationLimit", value);
    dispatch("fetchData", { id });
  },
  setSelectedStatuses: ({ commit }, { statuses }) => {
    commit("setSelectedStatus", statuses);
  }
};
const mutations = {
  fetchData: (state, payload) => {
    state.textList = payload.results;
    state.count = payload.count;
    state.loading = false;
  },
  setLoading: (state, payload) => {
    state.loading = payload.loading;
  },
  clear: state => {
    state.textList = null;
    state.currentPage = 1;
  },
  setCurrentPage: (state, payload) => {
    state.currentPage = payload;
  },
  setPaginationLimit: (state, payload) => {
    state.paginationLimit = payload;
  },
  setTextId: (state, payload) => {
    state.textId = payload;
  },
  setSelectedStatus: (state, payload) => {
    state.selectedStatuses = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
