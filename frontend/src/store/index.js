import Vue from "vue";
import Vuex from "vuex";
import auth from "./modules/auth";
import textList from "./modules/textList";
import user from "./modules/user";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  modules: {
    user,
    auth,
    textList
  },
  strict: debug
});
